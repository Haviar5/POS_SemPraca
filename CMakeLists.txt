cmake_minimum_required(VERSION 3.7)
project(POS_SemPraca)

set(CMAKE_CXX_STANDARD 14)

add_executable(POS_SemPraca main.cpp)